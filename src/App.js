import React, { useState } from 'react'
import styles from './App.module.css'


const symbolsToIgnore = ['%', '+', '/', '*']

function App() {

  const [state, setState] = useState([])
  const [isNewState, setIsNewState] = useState(false)
  const [savedValue, setSavedValue] = useState(0)

  const clear = () => {
  setState([])
  }

  const screen = () => {
    if(state.length === 0) return 0 
    else return state.join('') 
    }

  const calculate = (name) => {
    if(name === '%'){
    setState([eval(state.join(''))/100])
    } if (name === '='){
    setState([eval(state.join(''))])
    }
    setIsNewState(true)
  }

  const plusMinus = () => {
    if(state[0] === '-'){
      setState(state.slice(1))
    }
    else(
      setState(['-', ...state])
    )
  }

  const update = (e) => {
    if(isNewState) setState([e.target.name])
    else{
      if(state.length === 0 && symbolsToIgnore.includes(e.target.name) && state[0] === '-'){
        return
      }
      setState([...state, e.target.name])
    }
    setIsNewState(false)
  }

  const memoryFunc = (e) => {
    if(e.target.name === 'm+'){
      setSavedValue(savedValue + Number(state[state.length -1]))
    }
    if(e.target.name === 'm-'){
      setSavedValue(savedValue - Number(state[state.length -1]))
    }
    if(e.target.name === 'mr'){
      setState([savedValue])
    }
    if(e.target.name === 'mc'){
      setSavedValue(0)
    }
  }

  return (
    <div className={styles.calculator}>
      <div className={styles.monitor}>
        {screen()}
      </div>

      <div className={styles.greyButton}>
        <button name='AC' onClick={clear}>AC</button>
        <button name='+/-' onClick={plusMinus}>+/-</button>
        <button name='%' onClick={e => calculate(e.target.name)}>%</button>
      </div>

      <div className={styles.orangeButton}>
        <button name='/' onClick={update}>÷</button>
        <button name='m+' onClick={memoryFunc}>m+</button>
        <button name='*' onClick={update}>x</button>
        <button name='-' onClick={update}>-</button>
        <button name='+' onClick={update}>+</button>
        <button name='=' onClick={e => calculate(e.target.name)}>=</button>
      </div>

      <div className={styles.blackButton}>
        <div>
          <button name='mc' onClick={memoryFunc}>mc</button>
          <button className={savedValue && styles.mr} name='mr' onClick={memoryFunc}>mr</button>
          <button name='m-' onClick={memoryFunc}>m-</button>
        </div>
       <div>
        <button name='7' onClick={update}>7</button>
        <button name='8' onClick={update}>8</button>
        <button name='9' onClick={update}>9</button>
       </div>
       <div>
        <button name='4' onClick={update}>4</button>
        <button name='5' onClick={update}>5</button>
        <button name='6' onClick={update}>6</button>
       </div>
       <div>
        <button name='1' onClick={update}>1</button>
        <button name='2' onClick={update}>2</button>
        <button name='3' onClick={update}>3</button>
       </div>
        <button className={styles.nullButton} name='0' onClick={update}>0</button>
        <button name='.' onClick={update}>.</button>
      </div>
    </div>
  )
}

export default App